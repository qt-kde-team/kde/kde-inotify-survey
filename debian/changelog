kde-inotify-survey (24.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (24.12.0).
  * Update build-deps and deps with the info from cmake.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 21 Dec 2024 23:36:05 +0100

kde-inotify-survey (24.08.2-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * Fix Vcs-* URLs.
  * Fix upstream signing key.
  * Add/update Heiko Becker in upstream signing keys.
  * New upstream release (24.08.2).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 14 Oct 2024 00:46:33 +0200

kde-inotify-survey (24.05.2-1) experimental; urgency=medium

  [ Debian Qt/KDE Maintainers ]
  * New upstream release (24.05.2).

  [ Scarlett Moore ]
  * Update dependencies according to cmake.
  * Remove kf5 from rules.
  * Add hardening export to rules.
  * Refresh copyright.
  * Bump standards to version 4.7.0; no-changes-required.
  * Use minimal signing key.
  * Use https in watch file.

 -- Scarlett Moore <sgmoore@debian.org>  Tue, 06 Aug 2024 09:06:24 -0700

kde-inotify-survey (23.08.5-1) unstable; urgency=medium

  * Update Maintainer and uploaders.
  * Add VCS entires.
  * Add rules requires root: no.
  * Update copyright.
  * Don't run tests at build time.
  * Bump standards version to 4.6.2.

 -- Scarlett Moore <sgmoore@debian.org>  Wed, 21 Feb 2024 09:43:38 -0700

kde-inotify-survey (23.08.5-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 19 Feb 2024 15:51:10 +0000

kde-inotify-survey (23.08.4-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 06 Dec 2023 17:28:15 +0000

kde-inotify-survey (23.08.3-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 09 Nov 2023 15:57:45 +0000

kde-inotify-survey (23.08.2-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 11 Oct 2023 18:05:41 +0000

kde-inotify-survey (23.08.1-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 14 Sep 2023 01:53:59 +0000

kde-inotify-survey (23.08.0-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 24 Aug 2023 10:37:18 +0000

kde-inotify-survey (23.04.3-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 05 Jul 2023 12:39:12 +0000

kde-inotify-survey (23.04.2-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 06 Jun 2023 23:14:47 +0000

kde-inotify-survey (23.04.1-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 06 Sep 2022 15:47:38 +0000
